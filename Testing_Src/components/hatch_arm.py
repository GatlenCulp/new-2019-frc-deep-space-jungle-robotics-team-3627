import wpilib, ctre

off = 0
extended = 1
retracted = 2

class HatchArm:

    def __init__(self, robot):

        self.puncher = wpilib.DoubleSolenoid(
            forwardChannel=robot.map.pcm.PUNCHER_FORWARD, reverseChannel=robot.map.pcm.PUNCHER_BACKWARD
        )
        # Not sure what we are using for controlling the hatch arm
        self.angle_mc = ctre.WPI_VictorSPX(robot.map.can.HATCH_ARM_ANGLE_MC)
        self.angle_pot = wpilib.AnalogPotentiometer(robot.map.analog.HATCH_ARM_POT)

    def punch(self):
        self.puncher.set(extended)
        # print("Extended")

    def retract(self):
        self.puncher.set(retracted)
        # print("Retracted")

    def getAngle(self):
        return self.angle_pot.get()
        pass

    def adjustArm(self, speed):
        self.angle_mc.set(speed)
        pass
