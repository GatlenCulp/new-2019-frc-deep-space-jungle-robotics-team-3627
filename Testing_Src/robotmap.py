'''
This script serves to create a list of ports and channels that can be easily
changed if someone were to say- change the port a motor was plugged into on the
RoboRIO
'''

# List of ports for motor controllers used for driving on the RoboRIO (feeds into PWM)
class Drive:
    FR_CANTALON_MC = 3
    BL_CANTALON_MC = 2
    BR_CANTALON_MC = 4
    FL_CANTALON_MC = 5

# List of CAN ports on the RoboRIO (feeds into RobotMap)
class CAN:
    drive = Drive()
    BALL_ARM_ANGLE_LEFT_MC = 6
    BALL_ARM_ANGLE_RIGHT_MC = 7
    BALL_ARM_INTAKE_MC = 8
    HATCH_ARM_ANGLE_MC = 2 # Switched from Victor to Talon
    HAB_CLIMBER_ANGLE_MC = 10

# List of PWM ports on the RoboRIO (feeds into RobotMap)
class PWM:
    pass

# List of Pneumatic Control Module ports
class PCM:
    ''' FIX LATER '''
    PUNCHER_FORWARD = 0
    PUNCHER_BACKWARD = 1
    LEFT_HAB_CLIMBER = 2
    RIGHT_HAB_CLIMBER = 3

# FOUND XBOXCONTROLLER CODE, THIS IS NOW UNNECESSARY BUT I'M KEEPING IT IN THE
# CASE WE USE A FLIGHT CONTROLLER, JAVA, C++, HONESTLY JUST KIND OF USELESS
# List of buttons/triggers/analog sticks for the xbox controller (feeds into RobotMap)
class Joystick:
    A = 1
    B = 2
    X = 3
    Y = 4
    LB = 5
    RB = 6
    BACK = 7
    START = 8
    LS = 9
    RS = 10

    # These are analog inputs and are different from the other buttons above
    L_STICK_X = 0
    L_STICK_Y = 1
    # They're wrapped into one, LT is positive, RT is negative, they're added together
    TRIGGERS = 2
    R_STICK_Y = 3
    R_STICK_X = 4

    # Port on the laptop that goes to the Xbox controller
    XBOX_CONTROLLER_PORT = 0

class ANALOG:
    HATCH_ARM_POT = 0

# The full map/list of ports
class RobotMap:
    can = CAN()
    pwm = PWM()
    pcm = PCM()
    joystick = Joystick()
    analog = ANALOG()




# Used for testing if the RobotMap works
def test():
    print(RobotMap.pwm.drive.FR_CANTALON_MC)
    print(RobotMap.joystick.A)

# Doesn't run in final code
if __name__ == "__main__":
    test()
