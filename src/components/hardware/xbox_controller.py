import wpilib
# from pyconsole_util import util

'''
The robot has 3 different modes for the 3 functions of the robot
1. Hatch Arm (hatch)
2. Habitat Climber (hab)
3. Ball Arm (ball)

When you switch modes, your view, components you are controlling, and direction change for the robot

# SWITCHING MODES
X - Switch to hatch
Y - Switch to hab
B - Switch to ball

# HATCH MODE
LT - Bring arm up
RT - Bring arm down
Hold A - Extend puncher

# HAB MODE
LT - Bring arm up
RT - Bring arm down
A - Clamp/Release

# BALL MODE
LT - Aim down
RT - Aim up
Hold RB - Roll intake
Hold LB - Shoot ball

# DRIVING
Left Stick - Strafe
Right Stick - Rotate

# CONFIG MODE
Click Left Stick Down and hold to enter config mode
Move and release the left stick along the y axis to set the speed (dragging left stick all the way down and releasing is 0% power, all the way up is 100% power, and dead center is 50%. Good for fine movement on the fly)
Being able to flick and release this stick to get the speed you want quickly can be incredibly useful if mastered.

# WIP
Back arrow - switch drive mode to relative to field
Start arrow - switch drive mode to relative to robot
Click Right Stick - Reset gyro (for relative to field)
    # AUTONOMOUS MODES
    We think we might hold a button that is normally tapped and then use the left stick to select a target and head towards it
'''

class XboxController:

    def __init__(self, robot, port):
        self.xbox = wpilib.XboxController(port)
        self.robot = robot

    def deadZone(self, value, deadzone):
        if abs(value) < deadzone: return 0
        else: return value

    def read(self):
        # Any values not past the deadzone won't register because the stick isn't
        # perfectly straight when not touched
        self.analog_stick_deadzone = 0.20
        self.trigger_deadzone = 0.05

        self.a_just_pressed = self.xbox.getAButtonPressed()
        self.a_pressed = self.xbox.getAButton()
        self.a_released = self.xbox.getAButtonReleased()

        self.x_just_pressed = self.xbox.getXButtonPressed()
        self.x_pressed = self.xbox.getXButton()
        self.x_pressed_again = False


        self.y_just_pressed = self.xbox.getYButtonPressed()
        self.y_pressed = self.xbox.getYButton()
        self.y_pressed_again = False


        self.b_just_pressed = self.xbox.getBButtonPressed()
        self.b_pressed = self.xbox.getBButton()
        self.b_pressed_again = False

        self.start_just_pressed = self.xbox.getStartButtonPressed()
        self.back_just_pressed = self.xbox.getBackButtonPressed()

        self.left_bumper_pressed = self.xbox.getBumper(0)
        self.left_bumper_just_pressed = self.xbox.getBumperPressed(0)

        self.right_bumper_pressed = self.xbox.getBumper(1)
        self.right_bumper_just_pressed = self.xbox.getBumperPressed(1)

        self.left_trigger = self.deadZone(self.xbox.getTriggerAxis(0), self.trigger_deadzone)
        self.right_trigger = self.deadZone(self.xbox.getTriggerAxis(1), self.trigger_deadzone)

        self.left_stick_x = self.deadZone(self.xbox.getX(0), self.analog_stick_deadzone)
        self.left_stick_y = self.deadZone(self.xbox.getY(0), self.analog_stick_deadzone)
        self.right_stick_x = self.deadZone(self.xbox.getX(1), self.analog_stick_deadzone)
        self.right_stick_y = self.deadZone(self.xbox.getY(1), self.analog_stick_deadzone)

        self.left_stick_button_just_pressed = self.xbox.getStickButtonPressed(0)
        self.left_stick_button_pressed = self.xbox.getStickButton(0)
        self.left_stick_button_released = self.xbox.getStickButtonReleased(0)

        self.right_stick_button_just_pressed = self.xbox.getStickButtonPressed(1)

    def checkForPowerChange(self):
        if self.left_stick_button_just_pressed and not self.robot.drive.is_high_power: self.robot.drive.setPower(0.5)
        elif self.left_stick_button_just_pressed: self.robot.drive.setPower(1)

    def checkForModeChange(self):
        previous_mode = self.robot.mode

        if self.x_pressed: self.robot.mode = "hatch"
        elif self.y_pressed: self.robot.mode = "hab"
        elif self.b_pressed: self.robot.mode = "ball"

        if self.robot.mode != previous_mode:
            print("Changed mode to", self.robot.mode)
            self.x_pressed_again = False
            self.y_pressed_again = False
            self.b_pressed_again = False
        else:
            if self.robot.mode == "hatch" and self.x_pressed: self.x_pressed_again = True
            if self.robot.mode == "hab" and self.y_pressed: self.y_pressed_again = True
            if self.robot.mode == "ball" and self.b_pressed: self.b_pressed_again = True

    def checkForGyroCalibration(self):
        if self.right_stick_button_just_pressed:
            self.robot.gyro.reset()
