import ctre, wpilib
from components.abstractions import PowerOrientedArm # PowerOrientedArm
from components.hardware import RobotPotentiometer

retracted = 2
extended = 1

# TODO: Don't allow the ability to turn on pneumatics when below a certain point

def translate(value, leftMin, leftMax, rightMin, rightMax):
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    valueScaled = float(value - leftMin) / float(leftSpan)

    return rightMin + (valueScaled * rightSpan)

class HatchArmPower(PowerOrientedArm):

    is_folded = True

    def __init__(self, robot):
        
        self.puncher = wpilib.DoubleSolenoid(
            forwardChannel=robot.map.pcm.PUNCHER_FORWARD, reverseChannel=robot.map.pcm.PUNCHER_BACKWARD
        )

        super().__init__(
            robot,
            angling_motors = {"hatch_arm_angling_motor": ctre.WPI_VictorSPX(robot.map.can.HATCH_ARM_ANGLER)},
            angle_power_up = 0.8,
            angle_power_down = 0.5,
            # , actual_angle_per_reading=(237 - 124)
            potentiometer=RobotPotentiometer(robot.map.analog.HATCH_ARM_POTENTIOMETER, actual_angle_per_reading=(90 / (237 - 124)))
        )

        # 302 all the way folded, 237 90 degrees, 124 0 degrees
        # 237 - 124 = 114 per 90


    def unfold(self): pass

    def punch(self): self.puncher.set(extended)
        # Don't extend, arm is too far downward
        # if self.getAngle() < self.min_angle: return

    def retract(self):
        self.puncher.set(retracted)

    def operate(self):
        if self.robot.controller.a_pressed: self.punch()
        else: self.retract()

        user_input = self.robot.controller.left_trigger - self.robot.controller.right_trigger

        if user_input == 0: self.anchor()
        else: self.rotate(user_input)

    def update(self):
        # Has an encoder
        if self.encoder != None:
            self.angle = self.encoder.getAngle()
        # Has a potentiometer
        elif self.potentiometer != None:
            self.angle = translate(self.potentiometer.get(), 0.403, 1.006, 0, 171)

        # 0 = 0.403 171 = 1.006
        # print("Hatch arm value:", self.angle)
        # print("Hatch arm angle:", translate(self.angle, 0.403, 1.006, 0, 171))


